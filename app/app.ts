import { Component, OnInit, AfterViewInit } from "@angular/core"
import { APIClient } from "./services/apiclient.service"
import { User } from "./models/user"
import { UserService} from "./services/user.service"

declare var $

@Component({
    selector: "my-app",
    templateUrl: "app/app.html"
})
export class MyAppComponent implements OnInit, AfterViewInit {
    constructor(
        private _apiClient:APIClient,
        private _user_serv:UserService) {
    }

    ngOnInit() {
      let user = new User({
        firstname: "Alexander",
        lastname: "Pierce",
        email: "alexander.pierce@adminlte.com",
        avatar_url: "/node_modules/admin-lte/dist/img/user2-160x160.jpg"
      })
      this._user_serv.setCurrentUser( user )
    }

    ngAfterViewInit() {
         $.getScript("/node_modules/admin-lte/dist/js/app.js")
     }
}
