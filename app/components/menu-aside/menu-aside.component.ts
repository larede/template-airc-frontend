import { Component } from "@angular/core"
import { UserService } from "../../services/user.service"
import { User } from "../../models/user"

@Component({
  selector: "menu-aside",
  templateUrl: "app/components/menu-aside/menu-aside.component.html",
  styleUrls: ["app/components/menu-aside/menu-aside.component.css"]
})
export class MenuAsideComponent {
  private current_user: User

  constructor(
    private _user_serv:UserService) {
    this._user_serv.current_user.subscribe((user: User) => this.current_user = user)
  }
}
