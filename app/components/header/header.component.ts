import { Component, OnInit } from "@angular/core"
import { UserBoxComponent } from "../user-box/user-box.component"

@Component({
  selector: "app-header",
  templateUrl: "app/components/header/header.component.html",
  styleUrls: ["app/components/header/header.component.css"],
  directives: [UserBoxComponent]
})
export class HeaderComponent {
}
