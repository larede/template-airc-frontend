import { Component } from "@angular/core"
import { User } from "../../models/user"
import { UserService } from "../../services/user.service"
import { APIClient } from "../../services/apiclient.service"

@Component({
  selector: "user-box",
  templateUrl: "app/components/user-box/user-box.component.html",
  styleUrls: ["app/components/user-box/user-box.component.css"]
})
export class UserBoxComponent {
    current_user:User

    constructor(
      private _user_serv:UserService,
      private _apiClient: APIClient ) {
      this._user_serv.current_user.subscribe((user: User) => this.current_user = user)
    }

    logout() {
         this._apiClient.post(
           "logout",
           null,
           (data) => this.handleSuccessGoToLogin(data),
           (data) => this.handleErrorGoToLogin(data),
           () => {}
         )
     }

     handleSuccessGoToLogin(data:any) {
       window.location.href = "/"
     }

     handleErrorGoToLogin(data:any) {
       console.log(data)
     }
}
