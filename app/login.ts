import { Component } from "@angular/core"
import { Messages } from "primeng/primeng"
import { APIClient } from "./services/apiclient.service"

@Component({
    selector: "my-app",
    templateUrl: "app/login.html",
    styleUrls: ["app/login.css"],
    directives: [Messages]
})
export class LoginComponent {

    msgs: any[] = []

    constructor(private _apiClient: APIClient) {
    }

    // Faz pedido de authenticate ao serviço authenticate
    login(event: any, username: any, password: any) {
        let body = { "username": username, "password": password }
        this._apiClient.post(
            "login",
            body,
            (data) => this.handleSuccessAuthenticate(data),
            (data) => this.handleErrorAuthenticate(data),
            () => {}
            )
    }

    handleSuccessAuthenticate(data:any) {
      window.location.href = "/"
    }

    // HandleError ao pedido feito em login()
    handleErrorAuthenticate(data: any) {
        this.msgs.push({ severity: "error", summary: data.title, detail: data.message })
    }
}
