import {Injectable} from "@angular/core"
import {SelectItem} from "primeng/primeng"

declare let jQuery: any

@Injectable()
export class Utils {

    // Faz show de uma div com uma imagem que simula tempo de espera quando feito pedido
    showLoading() {
        jQuery("#divLoading").modal({ backdrop: "static", keyboard: "false" })
    }

    // Faz hide de uma div com uma imagem que simula tempo de espera quando feito pedido
    hideLoading() {
        jQuery("#divLoading").modal("hide")
    }

    changeTheme(tema: string) {
        let theme = tema, themeLink = jQuery("link[href$='theme.css']"), newThemeURL = "assets/lib/primeui/themes/" + theme + "/theme.css"
        themeLink.attr("href", newThemeURL)
    }

    getTemasDisponiveis(): Array<SelectItem> {
        let listaTemas: Array<string> = ["afterdark", "afternoon", "afterwork", "aristo", "black-tie", "bluesky", "bootstrap", "casablanca", "cruze", "dark-hive", "delta", "dot-luv", "eggplant", "excite-bike", "flick", "glass-x", "home", "hot-sneaks", "humanity", "le-frog", "midnight", "mint-choc", "omega", "overcast", "pepper-grinder", "redmond", "rocket", "sam", "smoothness", "south-street", "start", "sunny", "swanky-purse", "trontastic", "ui-darkness", "ui-lightness"]
        let tamanhoLista: number = listaTemas.length
        let temasDisponiveis: Array<SelectItem> = []
        for (let i = 0; i < tamanhoLista; i++) {
            temasDisponiveis.push({ label: listaTemas[i].toUpperCase(), value: listaTemas[i] })
        }
        return temasDisponiveis
    }
}
