import { Injectable } from "@angular/core"
import { Http, Headers, URLSearchParams, Response } from "@angular/http"
import { Observable } from "rxjs/Observable"
import "rxjs/add/operator/map"

declare let saveAs: any

export interface CallbackAPIClient {
    (data: string): void
}

@Injectable()
export class APIClient {

    constructor(private _http: Http) { }

    // Http request GET
    public get(resource: string, params: URLSearchParams, onSuccess: CallbackAPIClient, onError: CallbackAPIClient, typeOfCallFile?: boolean, opcoesFile?: any, onComplete?: any) {
        if (typeOfCallFile === undefined) {
            let httpResult = this._http.get(this.getUrl(resource), { body: "", search: params, headers: this.getHeaders() })
            this.handleResult(httpResult, onSuccess, onError, onComplete)
        } else {
            this.handleResultFile("GET", resource, onSuccess, onError, opcoesFile, onComplete)
        }
    }

    // Http request POST
    public post(resource: string, data: Object, onSuccess: CallbackAPIClient, onError: CallbackAPIClient, onComplete: CallbackAPIClient) {
        let httpResult: Observable<Response>

        if (data === null) {
            httpResult = this._http.post(this.getUrl(resource), "{}", { headers: this.getHeaders() })
        } else {
            httpResult = this._http.post(this.getUrl(resource), JSON.stringify(data), { headers: this.getHeaders() })
        }
        this.handleResult(httpResult, onSuccess, onError, onComplete)
    }

    // Http request PUT
    public put(resource: string, data: Object, onSuccess: CallbackAPIClient, onError: CallbackAPIClient, onComplete: CallbackAPIClient) {
        let httpResult: Observable<Response>

        if (data === null) {
            httpResult = this._http.put(this.getUrl(resource), "{}", { headers: this.getHeaders() })
        } else {
            httpResult = this._http.put(this.getUrl(resource), JSON.stringify(data), { headers: this.getHeaders() })
        }
        this.handleResult(httpResult, onSuccess, onError, onComplete)
    }

    // Http request DELETE
    public delete(resource: string, params: URLSearchParams, onSuccess: CallbackAPIClient, onError: CallbackAPIClient, onComplete: CallbackAPIClient) {
        let httpResult = this._http.delete(this.getUrl(resource),
            { body: "", search: params, headers: this.getHeaders() }
            )
        this.handleResult(httpResult, onSuccess, onError, onComplete)
    }

    // Handle Response dos http requests GET, POST, PUT, DELETE
    private handleResult(httpResult: Observable<Response>, onSuccess: CallbackAPIClient, onError: CallbackAPIClient, onComplete: CallbackAPIClient) {
        httpResult
            .map(res => res)
            .subscribe(
            data => {
                // Se status = NO_CONTENT (204)
                // não podemos executar a função data.json() porque não há conteudo
                if (data.status === 204) {
                    onSuccess("")
                } else {
                    if (data._body === "") {
                        onSuccess("")
                    } else {
                        onSuccess(data.json())
                    }
                }
            },
            error => {
                // Se não está autenticado
                if (error.status === 401) {
                    console.log(JSON.stringify(error))
                    location.reload()
                    // Se o recurso não existe
                } else if (error.status === 404) {
                    console.log(JSON.stringify(error))
                    let mensagemErro: any
                    mensagemErro = { message: "A página solicitada não existe.", title: "Recurso inexistente" }
                    onError(mensagemErro)
                    // Se é um erro da gama 500
                } else if (error.status.toString()[0] === "5") {
                    let mensagemErro: any
                    mensagemErro = { message: "Ocorreu um erro ao contactar o servidor, por favor tente novamente", title: "Falha na comunicação" }
                    onError(mensagemErro)
                } else {
                    console.log(JSON.stringify(error))
                    onError(JSON.parse(error._body))
                }
            },
            () => {
                onComplete("")
            }
            )
    }

    // Handle Response dos http requests GET de ficheiros
    private handleResultFile(tipoPedido: string, resource: string, onSuccess: CallbackAPIClient, onError: CallbackAPIClient, opcoesFile: any, onComplete: CallbackAPIClient) {
        // fazer get para file
        // Create the Xhr request object
        let xhr = new XMLHttpRequest()
        let url = this.getUrl(resource)
        xhr.open(tipoPedido, url, true)
        xhr.responseType = "blob"
        // Xhr callback when we get a result back
        // We are not using arrow function because we need the "this" context
        xhr.onreadystatechange = () => this.OnRStateChange(xhr, onSuccess, onError, opcoesFile, onComplete)
        // Start the Ajax request
        xhr.send()
    }

    // On change of status depois de pedido ser feito xhr.send()
    private OnRStateChange(xhr: XMLHttpRequest, onSuccess: CallbackAPIClient, onError: CallbackAPIClient, opcoesFile: any, onComplete: CallbackAPIClient) {
        // If we get an HTTP status OK (200), save the file using fileSaver
        // State Description (readyState)
        // 0 The request is not initialized
        // 1 The request has been set up
        // 2 The request has been sent
        // 3 The request is in process
        // 4 The request is complete
        let type = ""
        let terminationFile = ""
        if (opcoesFile.typeFile === "pdf") {
            type = "application/pdf"
            terminationFile = ".pdf"
        }
        if (opcoesFile.typeFile === "excel") {
            type = "application/excel"
            terminationFile = ".xls"
        }
        if (opcoesFile.typeFile === "ods") {
            type = "application/vnd.oasis.opendocument.spreadsheet"
            terminationFile = ".ods"
        }
        // ReadyState complete e status comeca por 2 ou seja OK
        if (xhr.readyState === 4 && xhr.status.toString()[0] === "2") {
            let blob = new Blob([xhr.response], { type: type })
            saveAs(blob, "Orcamento" + opcoesFile.tipoOrcamento + "_" + opcoesFile.anoEconomico + terminationFile)
            onSuccess("")
            onComplete("")
        }
        // Caso pedido completo e status not OK erro
        if (xhr.readyState === 4 && xhr.status.toString()[0] !== "2") {
            onError("")
            onComplete("")
        }
    }

    // Funcao que agrega ao resource a route inicial "http://localhost:9000/v1"
    private getUrl(resource: string) {
        return "http://localhost:9000/api/v1/" + resource
    }

    // Funcao que define os headers para os pedidos http request
    private getHeaders() {
        let headers = new Headers()
        headers.append("Accept", "application/json")
        headers.append("Content-Type", "application/json")
        return headers
    }
}
