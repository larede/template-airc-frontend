import { NgModule }       from "@angular/core"
import { BrowserModule }  from "@angular/platform-browser"
import { FormsModule }    from "@angular/forms"
import { HTTP_PROVIDERS } from "@angular/http"

import { HttpModule } from "@angular/http"

import { LoginComponent }   from "./login"

import { APIClient } from "./services/apiclient.service"

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  declarations: [
    LoginComponent
  ],
  providers: [
    HTTP_PROVIDERS, APIClient
  ],
  bootstrap: [ LoginComponent ]
})
export class LoginModule {
}
