import { NgModule }       from "@angular/core"
import { BrowserModule }  from "@angular/platform-browser"
import { FormsModule }    from "@angular/forms"
import { HTTP_PROVIDERS } from "@angular/http"

import { HttpModule } from "@angular/http"

import { MyAppComponent }   from "./app"
import { routing }        from "./app.routing"

import { DashboardComponent }   from "./components/dashboard/dashboard.component"
import { Page1Component }  from "./components/pages/page1.component"
import { Page2Component }  from "./components/pages/page2.component"
import { MenuAsideComponent } from "./components/menu-aside/menu-aside.component"
import { HeaderComponent } from "./components/header/header.component"

import { APIClient } from "./services/apiclient.service"
import { UserService } from "./services/user.service"

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    HttpModule
  ],
  declarations: [
    MyAppComponent,
    MenuAsideComponent,
    DashboardComponent,
    HeaderComponent,
    Page1Component,
    Page2Component
  ],
  providers: [
    HTTP_PROVIDERS, APIClient, UserService
  ],
  bootstrap: [ MyAppComponent ]
})
export class AppModule {
}
