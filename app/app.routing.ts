import { Routes, RouterModule } from "@angular/router"

import { DashboardComponent }  from "./components/dashboard/dashboard.component"
import { Page1Component }     from "./components/pages/page1.component"
import { Page2Component }     from "./components/pages/page2.component"

const appRoutes: Routes = [
  {
    path: "",
    redirectTo: "/dashboard",
    pathMatch: "full"
  },
  {
    path: "dashboard",
    component: DashboardComponent
  },
  {
    path: "page1",
    component: Page1Component
  },
  {
    path: "page2",
    component: Page2Component
  }
]

export const routing = RouterModule.forRoot(appRoutes)
